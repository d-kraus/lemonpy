#!/bin/python3
import subprocess
import sys
import threading
import datetime


class TimerClass(threading.Thread):
    def __init__(self, bar):
        threading.Thread.__init__(self)
        self.event = threading.Event()
        self.bar = bar

    def run(self):
        while not self.event.is_set():
            self.bar.draw()
            self.event.wait(20)


class LemonPy(object):
    tag_types_FG = {
        '#': '-',
        '+': '-',
        ':': '-',
        '!': '#141414',
        '.': '#ababab',
        '-': '#ababab',
        '%': '#ababab'
    }

    tag_types_BG = {
        '#': '-',
        '+': '#213941',
        ':': '-',
        '!': '#FF0675',
        '.': '-',
        '-': '-',
        '%': '-'
    }

    def __init__(self):
        self.bg = '#101010'
        self.fg = "#ffffff"
        self.bar = self.construct_bar()
        self.tag_types_BG['#'] = subprocess.getoutput(
            'herbstclient get'' window_border_active_color'
        )
        self.draw()

    def construct_bar(self):
            height = 16
            font = "Hack-Regular:size=9"
            y_offset, _, width, _ = subprocess.getoutput(
                'herbstclient monitor_rect'
            ).split(' ')
            subprocess.getoutput(f'herbstclient pad 0 {height}')
            return subprocess.Popen(
                f'lemonbar -g {width}x{height}+{y_offset}+0 -B{self.bg}'
                f' -F{self.fg} -f {font}',
                shell=True,
                stdin=subprocess.PIPE
            )

    def get_date(self):
        date = datetime.datetime.now()
        return f'%{{F-}}{date.strftime("%H:%M")}%{{F#ababab}}, '\
               f'{date.strftime("%a %b")}%{{F-}}{date.strftime(" %d")}'

    def handle_events(self):
        process = subprocess.Popen(
            ['herbstclient', '-i'],
            stdout=subprocess.PIPE
        )

        timer = TimerClass(bar=self)
        timer.start()
        while process.poll() is None:
            for line in process.stdout:
                event = line.decode('UTF-8').split('\t')[0].strip()
                if event == 'tag_changed' or event == 'tag_flags':
                    self.draw()

    def tag_changed(self):
        tags = subprocess.getoutput('herbstclient tag_status 0')\
                         .split('\t')[1:-1]

        out = []
        for tag in tags:
            fg = self.tag_types_FG[tag[0]]
            bg = self.tag_types_BG[tag[0]]

            out.append(f'%{{F{fg}}}%{{B{bg}}}{tag[1:]} ')

        out.append('%{B-}')
        return "".join(out)

    def draw(self):
        self.bar.stdin.write(
            f'%{{l}} {self.tag_changed()} %{{r}} {self.get_date()}\n'
            .encode('utf-8')
        )
        self.bar.stdin.flush()


if __name__ == "__main__":
    # check if we are already running
    result = int(subprocess.getoutput("ps aux | grep lemon2.py | wc -l"))
    if result > 3:
        sys.exit()

    bar = LemonPy()
    bar.handle_events()
